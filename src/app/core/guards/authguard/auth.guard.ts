import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { catchError, map, Observable, of } from 'rxjs';
import { GraphqlService } from '../../services/graphql/graphql.service';
import { User } from '../../models/user.interface';
import { ApolloQueryResult } from '@apollo/client';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  private readonly loginUrl = environment.authUrl;

  constructor(private readonly graphqlService: GraphqlService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.graphqlService.isAuth().pipe(
      map((response: ApolloQueryResult<User>) => {
        if (response?.data !== null) {
          return true;
        }
        window.location.href = this.loginUrl;
        return false;
      }),
      catchError(error => {
        return of(false);
      })
    );
  }
}
