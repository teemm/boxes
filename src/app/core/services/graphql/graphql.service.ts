import { Injectable } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import { catchError, EMPTY, map, Observable, of, pluck } from 'rxjs';
import { ApolloQueryResult } from '@apollo/client';
import { User } from '../../models/user.interface';
import { BoxWrapper } from '../../models/box.interface';

@Injectable({
  providedIn: 'root',
})
export class GraphqlService {
  constructor(private apollo: Apollo) {}

  isAuth(): Observable<ApolloQueryResult<User | any>> {
    return this.apollo
      .query({
        query: gql`
          query {
            currentUser {
              id
              name
              wallets {
                id
                amount
                currency
              }
            }
          }
        `,
      })
      .pipe(
        catchError((err: any) => {
          alert(`something happened wrong ${err}`);
          return EMPTY;
        })
      );
  }

  getBoxes(): Observable<ApolloQueryResult<any>> {
    return this.apollo
      .query({
        query: gql`
          query {
            boxes(free: false, purchasable: true, openable: true) {
              edges {
                node {
                  id
                  name
                  iconUrl
                  cost
                }
              }
            }
          }
        `,
      })
      .pipe(
        catchError((err: any) => {
          alert(`something happened wrong ${err}`);
          return EMPTY;
        })
      );
  }

  getBox(id: string): Observable<ApolloQueryResult<BoxWrapper>> {
    return this.getBoxes().pipe(
      pluck('data', 'boxes', 'edges'),
      map((items: any) => items.find((item: any) => item.node.id === id))
    );
  }

  openBox(id: string, amount: number = 1) {
    return this.apollo
      .mutate({
        mutation: gql`
          mutation openBox($id: ID!, $amount: Int) {
            openBox(input: { boxId: $id, amount: $amount }) {
              boxOpenings {
                id
                itemVariant {
                  id
                  name
                  value
                }
              }
            }
          }
        `,
        variables: {
          id,
          amount,
        },
      })
      .pipe(
        catchError((err: any) => {
          alert(`something happened wrong ${err}`);
          return EMPTY;
        })
      );
  }

  // TODO WS subscription doesn't works
  listenWalletUpdate() {
    return this.apollo
      .subscribe({
        query: gql`
          subscription OnUpdateWallet {
            updateWallet {
              wallet {
                id
                amount
                name
              }
            }
          }
        `,
      })
      .pipe(
        catchError((err: any) => {
          alert(`something happened wrong ${err}`);
          return of();
        })
      );
  }
}
