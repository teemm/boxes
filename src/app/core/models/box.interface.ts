export interface Box {
  cost: number;
  iconUrl: string;
  id: string;
  name: string;
}

export interface BoxWrapper {
  node: Box[] | Box;
}
