export interface User {
  data: currentUser;
  loading: boolean;
  networkStatus: number;
}

export interface currentUser {
  currentUser?: currentUserData | null;
}

export interface currentUserData {
  id: string;
  name: string;
  wallets: wallets[];
  __typename: string;
}

export interface wallets {
  amount: number;
  currency: string;
  id: string;
  __typename: string;
}
