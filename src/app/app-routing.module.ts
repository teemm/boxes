import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guards/authguard/auth.guard';

const routes: Routes = [
  {
    path: 'grids',
    loadChildren: () => import('./pages/grids/grids.module').then(m => m.GridsModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'grid',
    loadChildren: () => import('./pages/grid/grid.module').then(m => m.GridModule),
    canActivate: [AuthGuard],
  },
  {
    path: '',
    redirectTo: 'grids',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
