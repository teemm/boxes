import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GraphqlService } from './core/services/graphql/graphql.service';
import { Observable, share, Subject, takeUntil } from 'rxjs';
import { User } from './core/models/user.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  private readonly unsubscribe$: Subject<void> = new Subject<void>();
  title = 'application';
  authInfo$: Observable<User> = this.graphqlService
    .isAuth()
    .pipe(takeUntil(this.unsubscribe$), share());

  constructor(private readonly router: Router, private readonly graphqlService: GraphqlService) {}

  ngOnInit(): void {
    // TODO ws connection doesn't open
    this.graphqlService.listenWalletUpdate().subscribe(res => console.log(res));
  }

  onLogoClick(): void {
    this.router.navigate(['/']);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
