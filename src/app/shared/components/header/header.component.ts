import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { environment } from '../../../../environments/environment';
import { User } from '../../../core/models/user.interface';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnChanges {
  @Output() logoClickEvent: EventEmitter<void> = new EventEmitter<void>();
  @Input() userInfo: User | any;
  finalAmount: number = 0;

  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('userInfo') && this.userInfo) {
      // @ts-ignore
      this.userInfo?.data?.currentUser.wallets.forEach(item => {
        this.finalAmount += item.amount;
      });
    }
  }

  logIn(): void {
    window.location.href = environment.authUrl;
  }

  logOut(): void {
    window.location.href = environment.logOut;
  }

  logoClick(): void {
    this.logoClickEvent.emit();
  }
}
