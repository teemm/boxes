import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, share, Subject, switchMap, takeUntil } from 'rxjs';
import { GraphqlService } from '../../../../core/services/graphql/graphql.service';
import { BoxWrapper } from '../../../../core/models/box.interface';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainComponent implements OnDestroy {
  private readonly unsubscribe$: Subject<void> = new Subject<void>();
  box$: Observable<BoxWrapper | any>;
  results: any;
  loading: boolean = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly cdr: ChangeDetectorRef,
    private graphqlService: GraphqlService
  ) {
    this.box$ = this.route.paramMap.pipe(
      takeUntil(this.unsubscribe$),
      switchMap((data: any) => this.graphqlService.getBox(data?.params?.id)),
      share()
    );
  }

  openBox(box: any): void {
    this.loading = true;
    this.graphqlService
      .openBox(box?.node?.id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res: any) => {
        this.loading = res.loading;
        this.results = res.data;
        this.cdr.markForCheck();
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
