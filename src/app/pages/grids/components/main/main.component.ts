import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { GraphqlService } from '../../../../core/services/graphql/graphql.service';
import { Observable, pluck, share, Subject, takeUntil } from 'rxjs';
import { BoxWrapper } from '../../../../core/models/box.interface';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainComponent implements OnDestroy {
  private readonly unsubscribe$: Subject<void> = new Subject<void>();
  boxes$: Observable<BoxWrapper | any> = this.graphqlService
    .getBoxes()
    .pipe(pluck('data', 'boxes', 'edges'), takeUntil(this.unsubscribe$), share());

  constructor(private graphqlService: GraphqlService) {}

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
