import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GridsRoutingModule } from './grids-routing.module';
import { MainComponent } from './components/main/main.component';
import {SharedModule} from "../../shared/shared.module";
import {CoreModule} from "../../core/core.module";


@NgModule({
  declarations: [
    MainComponent
  ],
  imports: [
    CommonModule,
    GridsRoutingModule,
    SharedModule,
    CoreModule
  ]
})
export class GridsModule { }
