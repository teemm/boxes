export const environment = {
  production: true,
  authUrl: 'https://api-staging.csgoroll.com/auth/steam?redirectUri=http://localhost:4200',
  logOut: 'https://api-staging.csgoroll.com/logout?redirectUri=http://localhost:4200',
  baseUrl: 'https://api-staging.csgoroll.com/graphql',
  wsUrl: 'ws://api-staging.csgoroll.com/graphql',
};
